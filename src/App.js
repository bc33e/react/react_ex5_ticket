import logo from "./logo.svg";
import "./App.css";
import Ticket from "./Ex_Ticket/Ticket";

function App() {
  return (
    <div className="App">
      <Ticket />
    </div>
  );
}

export default App;
