import React, { Component } from "react";
import { connect } from "react-redux";
import { bookTicketAction } from "./redux/actions/ticketAction";

class Seat extends Component {
  renderSeat = () => {
    return this.props.hangGhe.danhSachGhe.map((ghe, index) => {
      let gheDaChon = "";
      let gheDangChon = "";
      let disabled = false;

      if (ghe.daDat) {
        gheDaChon = "gheDuocChon";
        disabled = true;
      }
      let viTriGheDangDat = this.props.bookingSeat.findIndex(
        (viTriGhe) => ghe.soGhe == viTriGhe.soGhe
      );
      if (viTriGheDangDat !== -1) {
        gheDangChon = "gheDangChon";
      }
      return (
        <button
          key={index}
          onClick={() => {
            this.props.handleBookingTicket(ghe);
          }}
          disabled={disabled}
          className={`ghe ${gheDaChon}${gheDangChon}`}
        >
          {ghe.soGhe}
        </button>
      );
    });
  };

  renderRow = () => {
    return this.props.hangGhe.danhSachGhe.map((hang, index) => {
      return (
        <button className="rowNumber" key={index}>
          {hang.soGhe}
        </button>
      );
    });
  };

  renderSeatRow = () => {
    if (this.props.soHang == 0) {
      return (
        <div className="ml-4">
          {this.props.hangGhe.hang}
          {this.renderRow()}
        </div>
      );
    } else {
      return (
        <div>
          {this.props.hangGhe.hang}
          {this.renderSeat()}
        </div>
      );
    }
  };

  render() {
    return (
      <div
        className="text-warning text-left mt-1 ml-5"
        style={{ fontSize: "15px" }}
      >
        {this.renderSeatRow()}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    bookingSeat: state.ticketReducer.bookingSeat,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleBookingTicket: (seat) => {
      dispatch(bookTicketAction(seat));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Seat);
