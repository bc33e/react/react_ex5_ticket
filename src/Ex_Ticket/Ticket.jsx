import React, { Component } from "react";
import Cart from "./Cart";
import Seat from "./Seat";
import "./BookingTicket.css";
import bgMovie from "../assets/HPbg.jpg";
import { connect } from "react-redux";

class Ticket extends Component {
  renderContent = () => {
    return this.props.dataSeat.map((hangGhe, index) => {
      return <Seat key={index} hangGhe={hangGhe} soHang={index} />;
    });
  };
  render() {
    return (
      <div
        className="bookingMovie"
        style={{
          position: "fixed",
          backgroundImage: `url(${bgMovie})`,
          width: "100%",
          height: "180%",
          backgroundSize: "100%",
        }}
      >
        <div
          style={{
            position: "fixed",
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(0,0,0,0.6)",
          }}
        >
          <div className="container-fluid">
            <div className="row">
              <div className="col-8 text-center">
                <h1 className="text-warning mt-3 mr-5">Đặt vé xem phim</h1>
                <h2 className="text-white mt-2 mr-5">Màn hình</h2>

                <div
                  className="mt-2"
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                  }}
                >
                  <div className="screen ml-5 mt-3"></div>
                  {this.renderContent()}
                </div>
              </div>
              <div className="col-4">
                <h3 className="text-light mt-5">Thông tin đặt vé</h3>
                <Cart />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dataSeat: state.ticketReducer.dataSeat,
  };
};

export default connect(mapStateToProps)(Ticket);
