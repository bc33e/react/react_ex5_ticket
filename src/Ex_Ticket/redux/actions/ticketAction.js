import {
  BOOK_TICKET,
  CANCEL_TICKET,
  CONFIRM_TICKET,
} from "../constants/ticketConstant";

export const bookTicketAction = (seat) => {
  return {
    type: BOOK_TICKET,
    payload: seat,
  };
};

export const cancelTicketAction = (seat) => {
  return {
    type: CANCEL_TICKET,
    payload: seat,
  };
};
export const confirmTicketAction = () => {
  return {
    type: CONFIRM_TICKET,
  };
};
