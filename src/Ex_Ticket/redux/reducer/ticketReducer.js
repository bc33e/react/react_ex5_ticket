import { data_seat } from "../../danhSachGhe";
import {
  BOOK_TICKET,
  CANCEL_TICKET,
  CONFIRM_TICKET,
} from "../constants/ticketConstant";

const initialState = {
  dataSeat: data_seat,
  bookingSeat: [],
};

export let ticketReducer = (state = initialState, action) => {
  switch (action.type) {
    case BOOK_TICKET: {
      let newBookingSeat = [...state.bookingSeat];
      let index = newBookingSeat.findIndex(
        (seat) => seat.soGhe == action.payload.soGhe
      );
      if (index == -1) {
        newBookingSeat.push(action.payload);
      } else {
        newBookingSeat.splice(index, 1);
      }
      state.bookingSeat = newBookingSeat;
      return { ...state };
    }

    case CANCEL_TICKET: {
      let newBookingSeat = [...state.bookingSeat];
      let index = newBookingSeat.findIndex(
        (seat) => seat.soGhe == action.payload
      );
      if (index != -1) {
        newBookingSeat.splice(index, 1);
      }
      state.bookingSeat = newBookingSeat;
      return { ...state };
    }

    case CONFIRM_TICKET: {
      let newDataSeat = [...state.dataSeat];
      let newBookingSeat = [...state.bookingSeat];
      newDataSeat.forEach((seatInfo) => {
        seatInfo.danhSachGhe.forEach((seat) => {
          state.bookingSeat.forEach((bookingSeat) => {
            if (bookingSeat.soGhe == seat.soGhe) {
              seat.daDat = true;
            }
          });
        });
      });

      state.dataSeat = newDataSeat;
      newBookingSeat = [];
      state.bookingSeat = newBookingSeat;
      return { ...state };
    }
    default:
      return state;
  }
};
