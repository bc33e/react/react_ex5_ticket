import React, { Component } from "react";
import { connect } from "react-redux";
import {
  cancelTicketAction,
  confirmTicketAction,
} from "./redux/actions/ticketAction";

class Cart extends Component {
  renderBookedTicket = () => {
    return this.props.bookingSeat.map((ghe, index) => {
      return (
        <tr key={index} className="text-light">
          <td>{ghe.soGhe}</td>
          <td>{ghe.gia.toLocaleString()}</td>
          <td className="text-center">
            <button
              className="text-danger"
              onClick={() => {
                this.props.hanldeCancelTicket(ghe.soGhe);
              }}
            >
              Hủy
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <div className="mt-3">
          <div></div>
          <button className="gheDuocChon"></button>
          <span style={{ fontSize: "20px" }} className="text-warning ml-2">
            Ghế được đặt
          </span>
          <br />
          <button className="gheDangChon"></button>
          <span style={{ fontSize: "20px" }} className="text-success ml-2">
            Ghế đang đặt
          </span>
          <br />
          <button className="ghe"></button>
          <span style={{ fontSize: "20px" }} className="text-white ml-2">
            Ghế chưa đặt
          </span>
        </div>
        <div className="mt-5">
          <table className="table bg bg-warning">
            <thead>
              <tr>
                <th>Số ghế</th>
                <th>Giá vé</th>
                <th>Thao tác</th>
              </tr>
            </thead>

            <tbody
              className="bg bg-secondary text-center"
              style={{ fontSize: "20px" }}
              border="2"
            >
              {this.renderBookedTicket()}
            </tbody>
          </table>

          <div>
            <div className="text-light" style={{ fontSize: "35px" }}>
              <div className="text-center">
                Tổng:
                {this.props.bookingSeat
                  .reduce((sum, ghe, index) => {
                    return (sum += ghe.gia);
                  }, 0)
                  .toLocaleString()}
              </div>
            </div>
          </div>
        </div>
        <button
          className="btn btn-info"
          onClick={() => {
            this.props.handleConfirmTicket();
          }}
        >
          Xác nhận đặt vé
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { bookingSeat: state.ticketReducer.bookingSeat };
};

const mapDispatchToProps = (dispatch) => {
  return {
    hanldeCancelTicket: (seat) => {
      dispatch(cancelTicketAction(seat));
    },
    handleConfirmTicket: () => {
      dispatch(confirmTicketAction());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
